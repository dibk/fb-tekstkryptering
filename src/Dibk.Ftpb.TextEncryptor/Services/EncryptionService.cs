﻿using Azure.Security.KeyVault.Keys;
using Azure.Security.KeyVault.Keys.Cryptography;
using Dibk.Ftpb.TextEncryptor.Models;
using Microsoft.Extensions.Options;
using System.Text;

namespace Dibk.Ftpb.TextEncryptor.Services
{
    public class EncryptionService : IEncryptionService
    {
        private readonly string _keyName;
        private readonly string _uri;
        private readonly ILogger<EncryptionService> _logger;
        private readonly KeyClient _keyClient;
        private readonly AzureCredentialsProvider _credentialsProvider;

        public EncryptionService(ILogger<EncryptionService> logger, IOptions<KeyVaultConfig> keyVaultConfig, KeyClient keyClient, AzureCredentialsProvider credentialsProvider)
        {
            _logger = logger;
            _keyClient = keyClient;
            _credentialsProvider = credentialsProvider;
            _keyName = keyVaultConfig.Value.KeyName;
            _uri = keyVaultConfig.Value.Uri;
        }

        public async Task<string> EncryptStringAsync(string input)
        {
            try
            {
                var cryptoClient = await CryptographyClient();

                byte[] inputAsByteArray = Encoding.UTF8.GetBytes(input);

                EncryptResult encryptResult = await cryptoClient.EncryptAsync(EncryptionAlgorithm.RsaOaep, inputAsByteArray).ConfigureAwait(false);

                return Convert.ToBase64String(encryptResult.Ciphertext);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred when encrypting {EncryptInput}", input);
                return string.Empty;
            }
        }

        public async Task<string> DecryptStringAsync(string input)
        {
            try
            {
                var cryptoClient = await CryptographyClient();

                byte[] inputAsByteArray = Convert.FromBase64String(input);

                DecryptResult decryptResult = await cryptoClient.DecryptAsync(EncryptionAlgorithm.RsaOaep, inputAsByteArray).ConfigureAwait(false);

                return Encoding.Default.GetString(decryptResult.Plaintext);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred when decrypting {DecryptInput}", input);
                return string.Empty;
            }
        }

        private async Task<CryptographyClient> CryptographyClient()
        {
            try
            {
                KeyVaultKey key = await _keyClient.GetKeyAsync(_keyName).ConfigureAwait(false);
                var cryptoClient = new CryptographyClient(key.Id, _credentialsProvider.GetAzureCredentials());
                return cryptoClient;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred when creating cryptographyClient");
                throw;
            }
        }
    }
}