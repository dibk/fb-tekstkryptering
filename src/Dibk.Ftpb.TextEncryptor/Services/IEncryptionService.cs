﻿namespace Dibk.Ftpb.TextEncryptor.Services
{
    public interface IEncryptionService
    {
        Task<string> EncryptStringAsync(string input);

        Task<string> DecryptStringAsync(string input);
    }
}