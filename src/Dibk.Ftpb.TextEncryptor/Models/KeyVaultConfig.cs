﻿namespace Dibk.Ftpb.TextEncryptor.Models
{
    public class KeyVaultConfig
    {
        public string Uri { get; set; }
        public string KeyName { get; set; }
    }
}