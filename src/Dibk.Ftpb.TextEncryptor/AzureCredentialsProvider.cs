﻿using Azure.Identity;
using Serilog;

namespace Dibk.Ftpb.TextEncryptor
{
    public class AzureCredentialsProvider
    {
        private readonly IConfiguration _configuration;

        public AzureCredentialsProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public DefaultAzureCredential GetAzureCredentials()
        {
            return GetAzureCredentials(_configuration);
        }

        public static DefaultAzureCredential GetAzureCredentials(IConfiguration configuration)
        {
            var tenantId = configuration["Azure:TenantId"];
            if (!string.IsNullOrWhiteSpace(tenantId))
            {
                Log.Logger.Information("Azure Auth: Uses tenantId {TenantId} for authentication", tenantId);
                return new DefaultAzureCredential(new DefaultAzureCredentialOptions() { TenantId = tenantId });
            }
            else
            {
                Log.Logger.Information("Azure Auth: No tenant configured, uses default");
                return new DefaultAzureCredential();
            }
        }
    }
}