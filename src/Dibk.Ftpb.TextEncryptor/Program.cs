using Dibk.Ftpb.TextEncryptor;
using Dibk.Ftpb.TextEncryptor.Models;
using Dibk.Ftpb.TextEncryptor.Services;
using Elastic.Apm.AspNetCore;
using Microsoft.Extensions.Azure;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddScoped<IEncryptionService, EncryptionService>();
builder.Services.AddScoped<AzureCredentialsProvider>();

builder.Services.Configure<KeyVaultConfig>(builder.Configuration.GetSection("KeyVault"));

builder.Services.AddHttpContextAccessor();
builder.Services.AddHealthChecks()
                    .AddAzureKeyVault(new Uri(builder.Configuration["KeyVault:Uri"]),
                                      AzureCredentialsProvider.GetAzureCredentials(builder.Configuration),
                                      opts => { opts.AddKey(builder.Configuration["KeyVault:KeyName"]); });

builder.Services.AddAzureClients(clientBuilder =>
{
    clientBuilder.AddKeyClient(new Uri(builder.Configuration["KeyVault:Uri"]))
        .WithCredential(AzureCredentialsProvider.GetAzureCredentials(builder.Configuration));
});

builder.Services.AddLogging(loggingBuilder =>
{
    loggingBuilder.AddSerilog();
});

builder.Host.UseSerilog();

var app = builder.Build();

SerilogConfiguration.ConfigureLogging(builder.Configuration);

app.UserCorrelationIdMiddleware();
app.UseSerilogRequestLogging();

app.UseElasticApm(builder.Configuration);

app.UseHealthChecks(new PathString("/health"));

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();