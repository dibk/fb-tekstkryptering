﻿using Dibk.Ftpb.TextEncryptor.Models;
using Dibk.Ftpb.TextEncryptor.Services;
using Microsoft.AspNetCore.Mvc;

namespace Dibk.Ftpb.TextEncryptor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EncryptionController : ControllerBase
    {
        private IEncryptionService _encryptionService;

        public EncryptionController(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        [HttpGet]
        [Route("encrypt")]
        public async Task<ActionResult> Encrypt(string cryptText)
        {
            if (string.IsNullOrEmpty(cryptText)) return NoContent();
            var encryptedString = await _encryptionService.EncryptStringAsync(cryptText);
            if (string.IsNullOrEmpty(encryptedString)) return BadRequest("Kunne ikke kryptere");
            return Ok(encryptedString);
        }

        [HttpPost]
        [Route("decrypt")]
        public async Task<ActionResult> Decrypt(Encryption cryptText)
        {
            if (cryptText == null) return NoContent();
            var decryptedString = await _encryptionService.DecryptStringAsync(cryptText.CryptText);
            if (string.IsNullOrEmpty(decryptedString)) return BadRequest("Kunne ikke dekryptere");
            return Ok(decryptedString);
        }
    }
}